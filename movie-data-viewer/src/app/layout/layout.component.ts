import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  @Input() isMenuOpened = false;
  @Output() isMenuOpenedChange = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }

}
