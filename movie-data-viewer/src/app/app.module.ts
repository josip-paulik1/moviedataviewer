import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout.component';
import { SidebarModule } from 'primeng/sidebar';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { MoviesOverviewComponent } from './movies-overview/movies-overview.component';
import { MoviesDetailCardComponent } from './movies-detail-card/movies-detail-card.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { InputTextModule } from 'primeng/inputtext';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorPageComponent } from './error-page/error-page.component';
import { CacheInterceptor } from './cache.interceptor';
import { TooltipModule } from 'primeng/tooltip';
import { FormsModule } from '@angular/forms';
import { BadgeModule } from 'primeng/badge';
import { WishlistComponent } from './wishlist/wishlist.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    LayoutComponent,
    MoviesOverviewComponent,
    MoviesDetailCardComponent,
    MovieDetailComponent,
    ErrorPageComponent,
    WishlistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SidebarModule,
    MenubarModule,
    BrowserAnimationsModule,
    ButtonModule,
    InputTextModule,
    CardModule,
    HttpClientModule,
    TooltipModule,
    FormsModule,
    BadgeModule,
    RouterModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CacheInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
