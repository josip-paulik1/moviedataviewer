import { Inject, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable, of, tap } from 'rxjs';

@Injectable()
export class CacheInterceptor implements HttpInterceptor {

  private cache: Map<string, HttpResponse<any>> = new Map()

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.method !== "GET") {
      return next.handle(request);
    }
    const urlWithParams = request.urlWithParams;
    const cachedResponse = this.cache.get(urlWithParams);

    if (cachedResponse) {
      return of(cachedResponse.clone());
    }
    else {
      return next.handle(request).pipe(
        tap(stateEvent => {
          if (stateEvent instanceof HttpResponse) {
            this.cache.set(urlWithParams, stateEvent.clone());
          }
        })
      );
    }


  }
    
}
