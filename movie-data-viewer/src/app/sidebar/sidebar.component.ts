import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SidebarModule } from 'primeng/sidebar';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() isMenuOpened: boolean = false;
  @Output() isMenuOpenedChange = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }

  onCloseSidebar() {
    this.isMenuOpenedChange.emit(this.isMenuOpened);
  }
}
