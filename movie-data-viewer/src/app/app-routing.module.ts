import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './error-page/error-page.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MoviesOverviewComponent } from './movies-overview/movies-overview.component';
import { WishlistComponent } from './wishlist/wishlist.component';

const routes: Routes = [
  {path: '', redirectTo:'movies', pathMatch: 'full'},
  {path: 'movies', component: MoviesOverviewComponent},
  {path: 'movies/:id', component: MovieDetailComponent},
  {path: 'wishlist', component: WishlistComponent},
  {path: '**', component: ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
