import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesDetailCardComponent } from './movies-detail-card.component';

describe('MoviesDetailCardComponent', () => {
  let component: MoviesDetailCardComponent;
  let fixture: ComponentFixture<MoviesDetailCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoviesDetailCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesDetailCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
