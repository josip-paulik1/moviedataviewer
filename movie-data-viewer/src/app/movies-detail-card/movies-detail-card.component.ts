import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Movie } from '../models/movie';
import { WishlistService } from '../services/wishlist.service';
import{ RouterModule } from '@angular/router';

@Component({
  selector: 'app-movies-detail-card',
  templateUrl: './movies-detail-card.component.html',
  styleUrls: ['./movies-detail-card.component.css']
})
export class MoviesDetailCardComponent implements OnInit {
  @Input() movie!: Movie;

  getImageUrlPrefix: string = environment.imagePrefix + '/w300';

  constructor(private wishlistService: WishlistService) { }

  ngOnInit(): void {
  }

  isMovieInWishlist(id: number): boolean {
    return this.wishlistService.isMovieInWishlist(id);
  }

  addMovieToWishlist(movie: Movie) {
    this.wishlistService.addMovie(movie);
  }

  removeMovieFromWishlist(id: number) {
    this.wishlistService.removeMovie(id);
  }

}
