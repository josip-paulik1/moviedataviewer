import { Component, OnInit } from '@angular/core';
import { Movie } from '../models/movie';
import { WishlistService } from '../services/wishlist.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {

  get movies(): Array<Movie> {
    return this.wishlistService.getAllMovies();
  }
  constructor(private wishlistService: WishlistService) { }

  ngOnInit(): void {
  }

}
