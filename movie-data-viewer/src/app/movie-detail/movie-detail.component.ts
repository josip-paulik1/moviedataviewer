import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CastMember } from '../models/cast-member';
import { MovieDetail } from '../models/movie-detail';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class MovieDetailComponent implements OnInit {

  movie!: MovieDetail;
  cast!: CastMember[];
  getImageUrlPrefix: string = environment.imagePrefix + '/w300';
  actorImageUrlPrefix: string = environment.imagePrefix + '/w200';
  productionCompanyLogoUrlPrefix: string  = environment.imagePrefix + '/w200';
  containerBackgroundImage!: string;
  constructor(private route: ActivatedRoute, private router: Router, private movieService: MovieService, private titleService: Title) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.movieService.getMovie(Number(params.get('id')));
      })
    ).subscribe(response => { 
      this.movie = response;
      this.titleService.setTitle(this.movie.title);
      this.containerBackgroundImage = `linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url("${environment.imagePrefix}/original${this.movie.backdrop_path}")`;
    }, error => {
      
    });

    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.movieService.getMovieCast(Number(params.get('id')));
      })
    ).subscribe(response => { 
      this.cast = response.cast;
      this.cast = this.cast.sort(x => x.order);
    });
  }

}
