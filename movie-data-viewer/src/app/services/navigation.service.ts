import { Location } from '@angular/common';
import { ConditionalExpr } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  /**
   * How many items are in history
   */
  private historyStateTotal: number = 0;

  /**
   * Where are we in history
   */
  private currentHistoryState = 0;

  private lastNavigationId = 0;

  constructor(private location: Location, private router: Router) { 
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if (event.navigationTrigger == "popstate") {
          if (this.lastNavigationId > (event.restoredState?.navigationId ?? 0)) {
            this.currentHistoryState--;
          }
          else {
            this.currentHistoryState++;
          }
          this.lastNavigationId = event.restoredState?.navigationId ?? 0;
          return;
        }

        if (this.currentHistoryState < this.historyStateTotal) {
          this.historyStateTotal = this.currentHistoryState;
        }
        this.historyStateTotal++;
        this.currentHistoryState++;
        this.lastNavigationId = event.id;
      }
    })
  }

  goBack() {
    if (!this.canGoBack()) {
      return;
    }
    this.location.back();
  }

  goForward() {
    if (!this.canGoForward()) {
      return;
    }
    this.location.forward();
  }

  canGoBack(): boolean {
    return this.currentHistoryState - 1 > 0
  }

  canGoForward(): boolean {
    return this.currentHistoryState < this.historyStateTotal
  }
}
