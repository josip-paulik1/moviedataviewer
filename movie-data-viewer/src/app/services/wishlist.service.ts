import { Injectable } from '@angular/core';
import { Movie } from '../models/movie';
import { MovieDetailComponent } from '../movie-detail/movie-detail.component';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  constructor() { 
  }

  addMovie(movie: Movie): void {
    let movieMap = this.getMovieMap();
    movieMap.set(movie.id, movie);
    localStorage.setItem("wishlist", JSON.stringify(Array.from(movieMap.entries())));
  }

  getMovie(id: number): Movie | undefined {
    let movieMap = this.getMovieMap();

    return movieMap.get(id);
  }

  getAllMovies(): Array<Movie> {
    let movieMap = this.getMovieMap();
    return Array.from(movieMap.values());
  }

  getNumberOfMovies(): number {
    let movieMap = this.getMovieMap();

    return movieMap.size;
  }

  removeMovie(id: number): void {
    let movieMap = this.getMovieMap();
    if (!movieMap.has(id)) {
      return;
    }

    movieMap.delete(id);
    localStorage.setItem("wishlist", JSON.stringify(Array.from(movieMap.entries())));
  }

  getMovieMap(): Map<number, Movie> {
    let movieMapJSON = window.localStorage.getItem('wishlist') ?? "";
    let movieMap = new Map<number, Movie>();
    if (movieMapJSON && movieMapJSON !== "{}") {
      movieMap = new Map(JSON.parse(movieMapJSON));
    }
    
    return movieMap;
  }

  isMovieInWishlist(id: number): boolean {
    let movieMap = this.getMovieMap();

    return movieMap.has(id);
  }
}
