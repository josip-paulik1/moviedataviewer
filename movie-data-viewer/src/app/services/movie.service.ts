import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MovieResult } from '../models/movie-result';
import { MovieDetail } from '../models/movie-detail';
import { MovieCastResult } from '../models/movie-cast-result';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  baseUrl = environment.baseMovieUrl;
  apiKey = environment.apiKey;
  constructor(private httpClient: HttpClient) { }

  getMovies(page: number = 1): Observable<MovieResult> {
    let url = `${this.baseUrl}/discover/movie`;
    let params = new HttpParams();
    params = params.set('api_key', this.apiKey);

    // Job interviews don't need to be akward :)
    params = params.set('include_adult', false);
    params = params.set('page', page);
    
    return this.httpClient.get<MovieResult>(url, {
      params: params
    });
  }

  getMovie(id: number): Observable<MovieDetail> {
    let url = `${this.baseUrl}/movie/${id}`;
    let params = new HttpParams();
    params = params.set('api_key', this.apiKey);

    return this.httpClient.get<MovieDetail>(url, {
      params: params
    });
  }

  getMovieCast(id: number): Observable<MovieCastResult> {
    let url = `${this.baseUrl}/movie/${id}/credits`;
    let params = new HttpParams();
    params = params.set('api_key', this.apiKey);

    return this.httpClient.get<MovieCastResult>(url, {
      params: params
    });
  }

}
