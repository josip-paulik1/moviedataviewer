import { CastMember } from "./cast-member";
import { Genre } from "./genre";
import { ProductionCompany } from "./production-company";

export interface MovieDetail {
    backdrop_path: string;
    budget: number;
    genres: Genre[];
    id: number;
    overview: string;
    poster_path: string;
    production_companies: ProductionCompany[];
    release_date: string;
    revenue: number;
    status: string;
    title: string;
    vote_average: number;
}