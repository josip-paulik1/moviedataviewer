export interface Movie {
    poster_path: string;
    overview: string;
    release_date: string;
    genre_ids: number[];
    id: number;
    title: string;
    backdrop_path: string;
    popularity: number;
    vote_count: number;
    vote_average: number;
}