import { Movie } from "./movie";

export interface MovieResult {
    page: number;
    results: Array<Movie>;
    total_pages: number;
}