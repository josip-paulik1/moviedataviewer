import { CastMember } from "./cast-member";

export interface MovieCastResult {
    id: number;
    cast: CastMember[];
}