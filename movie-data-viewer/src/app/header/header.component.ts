import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Location } from '@angular/common';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { MenuItem } from 'primeng/api';
import { WishlistService } from '../services/wishlist.service';
import { Router } from '@angular/router';
import { NavigationService } from '../services/navigation.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  
  @Output() isMenuOpenedChange = new EventEmitter<boolean>();
  @Input() isMenuOpened = false;
  get menuIcon(): string {
    return this.isMenuOpened ? 'pi pi-chevron-right' : 'pi pi-chevron-left';
  }
  get buttonClass(): string {
    return this.isMenuOpened ? 'p-button-danger' : 'p-button-primary';
  }

  get wishlistSize(): string {
    return this.wishlistService.getNumberOfMovies().toString();
  }

  constructor(private wishlistService: WishlistService, private navigationService: NavigationService) { }

  ngOnInit(): void {
  }

  showSidebar(): void { 
    this.isMenuOpened = true; 
    this.isMenuOpenedChange.emit(this.isMenuOpened);
  }

  goBack(): void {
    this.navigationService.goBack();
  }

  goForward(): void {
    this.navigationService.goForward();
  }

  get cantGoBack(): boolean {
    return !this.navigationService.canGoBack();
  }

  get cantGoForward(): boolean {
    return !this.navigationService.canGoForward();
  }

  get backButtonClassList(): string {
    return this.cantGoBack ? ["p-button-rounded", "p-button-secondary"].join(" ") : "p-button-rounded";
  }

  get forwardButtonClassList(): string {
    return this.cantGoForward ? ["p-button-rounded", "p-button-secondary"].join(" ") : "p-button-rounded";
  }


}
