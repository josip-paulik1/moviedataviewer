import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { InputTextModule } from 'primeng/inputtext';
import { Movie } from '../models/movie';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-movies-overview',
  templateUrl: './movies-overview.component.html',
  styleUrls: ['./movies-overview.component.css']
})
export class MoviesOverviewComponent implements OnInit {
  movies: Array<Movie> = [];
  public moviesLoaded = false;
  _currentPage = 1;
  get currentPage(): number {
    return this._currentPage;
  }
  set currentPage(value) {
    if (value < 1) {
      value = 1
    }
    this._currentPage = value;
  }
  
  maximumPage = 0;
  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.loadMovies(this.currentPage);
    
  }

  goToFirstPage(): void {
    this.currentPage = 1;
    this.loadMovies(this.currentPage);
  }

  goToPreviousPage(): void {
    this.currentPage--;
    this.loadMovies(this.currentPage);
  }

  goToPage(): void {
    this.loadMovies(this.currentPage);
  }

  goToNextPage(): void {
    this.currentPage++;
    this.loadMovies(this.currentPage);
  }

  goToLastPage(): void {
    this.currentPage = this.maximumPage;
    this.loadMovies(this.currentPage);
  }

  

  loadMovies(page: number): void {
    this.moviesLoaded = false;
    setTimeout(() => {
      this.movieService.getMovies(page).subscribe(response => {
        this.movies = response.results;
        this.moviesLoaded = true;
        this.maximumPage = response.total_pages;
      });
    }, 2000);
  }

}
